#include "Samochod.h"

int PorRok(Samochod * jeden, Samochod * dwa)
{
    if(jeden->rok > dwa->rok)
        return -1;
    if(jeden->rok == dwa->rok)
        return 0;
    else
        return 1;
}

int PorCena(Samochod * jeden, Samochod * dwa)
{
    if(jeden->cena > dwa->cena)
        return -1;
    if(jeden->cena == dwa->cena)
        return 0;
    else
        return 1;
}
int PorPrzeb(Samochod * jeden, Samochod * dwa)
{
    if(jeden->przebieg > dwa->przebieg)
        return -1;
    if(jeden->przebieg == dwa->przebieg)
        return 0;
    else
        return 1;
}

int PorMod(Samochod * jeden, Samochod * dwa)
{
    if(strcmp(jeden->model, dwa->model)< 0)
        return -1;
    if(strcmp(dwa->model, jeden->model)== 0)
        return 0;
    else
        return 1;
}

int PorWlas(Samochod * jeden, Samochod * dwa)
{
    if(strcmp(jeden->wlas, dwa->wlas)< 0)
        return -1;
    if(strcmp(dwa->wlas, jeden->wlas)== 0)
        return 0;
    else
        return 1;
}

int PorMar(Samochod * jeden, Samochod * dwa)
{
    if(strcmp(jeden->typ.nazwa, dwa->typ.nazwa)< 0)
        return -1;
    if(strcmp(dwa->typ.nazwa, jeden->typ.nazwa)== 0)
        return 0;
    else
        return 1;
}
