Repozytorium zawiera projekt prostej bazy danych dla salonu samochodowego.

Cały projekt jest napisany z wykorzystaniem języka C.

Był to jeden z pierwszych projektów, dlatego zawiera kilka niedociągnięć, między innymi inną niż ogólnie przyjętą
stylistykę dotyczącą nazw zmiennych, funkcji itd.

Aplikacja uruchamia się w konsoli i pozwala wczytywać oraz zapisywać bazy danych do plików .txt.

Po wczytaniu pozwala dodawać, modyfikować, sortować oraz usuwać poszczególne rekordy.