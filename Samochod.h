#ifndef SAMOCHOD_H_INCLUDED
#define SAMOCHOD_H_INCLUDED

#include "Marka.h"

#define DL_TXT 25

struct Samochod{
    struct Samochod * nast;
    int rok;
    int cena;
    int przebieg;
    char wlas[DL_TXT];
    char model[DL_TXT];
    Marka typ;
};

typedef struct Samochod Samochod;

void PokazBaze();
void Dodaj();
void Dodaj();
void SprawdzTekst();
void UsunSamochod();
void EdytSamochod();
void Sort();
void CzyscBaze();
void ZapiszBaze();
void AktualizujBaze();
Samochod * WczytajBaze();
Samochod WczytajElement();

#endif // SAMOCHOD_H_INCLUDED
