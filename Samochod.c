#include "Samochod.h"
#include "Funkcje.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

void Dodaj(Samochod ** glowa, Samochod *nowy)
{
    Samochod * temp = malloc(sizeof(Samochod));
    Samochod ** ost = malloc(sizeof(Samochod));
    for(ost = &*glowa; *ost; ost = &(*ost)->nast);

    temp->cena = nowy->cena;
    strcpy(temp->model,nowy->model);
    temp->przebieg = nowy->przebieg;
    temp->rok = nowy->rok;
    //strcpy(temp->typ->nazwa ,nowy.typ->nazwa);
    strcpy(temp->wlas, nowy->wlas);
    temp->typ = nowy->typ;
    *ost = temp;
    temp->nast = NULL;
}

Samochod* WczytajBaze(char NazwaPlik[])
{
    Samochod *glowa = malloc(sizeof(Samochod));
    glowa = NULL;
    FILE *plik;
    plik = fopen(NazwaPlik, "r");
    Samochod  wczyt;
    if(!plik)
    {
        printf("Blad\n");
        return NULL;
    }
    else
    {
        while(fscanf(plik, "%s %s %d %d %s %d",wczyt.typ.nazwa , wczyt.model, &wczyt.rok, &wczyt.cena, wczyt.wlas, &wczyt.przebieg) == 6)
        {
            Dodaj(&glowa, &wczyt);
        }
    }
    fclose(plik);
    return glowa;
}

void PokazBaze(Samochod * glowa)
{
    if(!glowa)
    {
        puts("Baza pusta");
        return;
    }
    Samochod * temp = malloc(sizeof(Samochod));
    temp = glowa;
    puts("###############################################################################");
    puts("##############################Prezentacja bazy#################################");
    printf("MARKA       MODEL        ROK          CENA          WLAS     PRZEBIEG\n");
    puts("###############################################################################");
    int i = 1;
    while(temp)
    {
        printf("%-10s %6s \t %-8d",
               temp->typ.nazwa, temp->model, temp->rok);
               PrtDuze(temp->cena);
               printf("  %10s", temp->wlas);
               PrtDuze(temp->przebieg);
               printf(" || %d\n", i);
        temp = temp->nast;
        i++;
    }
    free(temp);
}

Samochod WczytajElement()
{
    const int ROK_MAX = 2018;
    const int ROK_MIN = 1950;
    const int CENA_MAX = 1000000;
    const int CENA_MIN = 1000;
    const int PRZEBIEG_MAX = 2000000;
    const int PRZEBIEG_MIN = 1000;
    Marka * Baza = malloc(sizeof(Marka));
    Baza = WczytajMarka("Mark.txt");
    Samochod nowy;

    puts("Podaj nazwe marki");
    SprawdzMarka(&nowy.typ, Baza);

    puts("Podaj nazwe modelu");
    scanf("%s", nowy.model);
    getchar();

    puts("Podaj rocznik");
    printf("Podaj liczbe z przedzialu %d - %d\n", ROK_MIN, ROK_MAX );
    nowy.rok = Sprawdz(ROK_MIN, ROK_MAX);

    puts("Podaj cene");
    pokapr("Podaj liczbe z przedzialu ", CENA_MIN, CENA_MAX);
    nowy.cena = sprawdzKomende(CENA_MIN, CENA_MAX);

    puts("Podaj nazwe wlasciciela");
    SprawdzTekst(nowy.wlas);

    puts("Podaj przebieg");
    pokapr("Podaj liczbe z przedzialu ", PRZEBIEG_MIN, PRZEBIEG_MAX);
    nowy.przebieg = sprawdzKomende(PRZEBIEG_MIN, PRZEBIEG_MAX);

    puts("Udalo sie wczytac element. Wcisnij dowolny klawisz aby kontynulowac");
    printf("%s %s %d %d %s %d \n",nowy.typ.nazwa, nowy.model, nowy.rok, nowy.cena, nowy.wlas, nowy.przebieg);
    getchar();
    free(Baza);
    return nowy;
}

void SprawdzTekst(char model[])
{
    scanf("%s", model);
    getchar();
    for(int i=0; model[i]; i++)
    {
        if(isdigit(model[i]))
        {
            puts("Podaj nazwe bez cyfr");
            SprawdzTekst(model);
        }

    }
}

void ZapiszBaze(char NazwaPlik[DL_TXT],Samochod * glowa)
{
    FILE *plik;
    plik = fopen(NazwaPlik, "w");
    Samochod * temp = malloc(sizeof(Samochod));
    temp = glowa;
    if(!plik)
    {
        printf("Blad\n");
    }
    else
    {
        while(temp)
        {
            fprintf(plik, "%s %s %d %d %s %d\n",
                    temp->typ.nazwa, temp->model, temp->rok, temp->cena,temp->wlas, temp->przebieg);
            temp = temp->nast;
        }
    }
    free(temp);
    puts("Zapisano");
    getchar();
    fclose(plik);
}

void Sort(Samochod ** glowa, int (*fun)())
{
    if(!*glowa)
    {
        puts("blad");
        return;
    }
    Samochod ** temp1 = malloc(sizeof(Samochod));
    Samochod * temp2 = malloc(sizeof(Samochod));
    temp1=glowa;
    temp2=(*temp1)->nast;
    int w=0;
    while(temp2)
    {
        if((*fun)(((*temp1)) , temp2)==1)
        {
            (*temp1)->nast=temp2->nast;
            temp2->nast = (*temp1);
            (*temp1) = temp2;
            temp2=temp2->nast->nast;
            w=1;
        }
        else
        temp2=temp2->nast;//else
        temp1=&(*temp1)->nast;//zawsze
    }
    if(w)
        Sort(glowa, fun);
}

void UsunSamochod( Samochod ** glowa, int i)
{
    if(!i)
        return;
    if(!(*glowa)){
            puts("Baza pusta. Blad");
        return;
    }
    Samochod ** temp;
    int a = 1;
    for(temp = &*glowa; *temp; temp = &(*temp)->nast)
    {
        Samochod * obecny = *temp;
        if(a == i)
        {
            *temp = obecny -> nast;
            free(obecny);
            getchar();
            return;
        }
        a++;
    }
    puts("Podales za duza liczbe");
}

void EdytSamochod( Samochod ** glowa, int i, int komenda)
{
    if(!*glowa)
    {
        puts("Zla glowa");
        return;
    }
    if(!i){
        puts("Zly numer elementu");
        return;
    }
    if(!komenda){
            puts("Zla komenda");
        return;
    }
    Samochod * temp;
    int a = 1;
    for(temp = *glowa; a < i; temp =temp->nast)
    {
        a++;
        if(!(temp->nast)){
            puts("Zly numer elementu!");
            return;
        }
    }
    const int ROK_MAX = 2018;
    const int ROK_MIN = 1950;
    const int CENA_MAX = 1000000;
    const int CENA_MIN = 1000;
    const int PRZEBIEG_MAX = 2000000;
    const int PRZEBIEG_MIN = 100;
    Marka * BazaMarki = WczytajMarka("Mark.txt");
    Samochod nowy;

    if(komenda == 1){
    SprawdzMarka(&nowy.typ, BazaMarki);
    strcpy(temp->typ.nazwa, nowy.typ.nazwa);
    }

    if(komenda == 2){
    puts("Podaj nazwe modelu");
    SprawdzTekst(nowy.model);
    strcpy(temp->model, nowy.model);
    }

    if(komenda == 3){
    puts("Podaj rokcznik");
    nowy.rok = Sprawdz(ROK_MIN, ROK_MAX);
    temp->rok = nowy.rok;
    }

    if(komenda == 4){
    puts("Podaj cene");
    nowy.cena = sprawdzKomende(CENA_MIN, CENA_MAX);
    temp -> cena = nowy.cena;
    }

    if(komenda == 5){
    puts("Podaj nazwe wlasciciela ");
    SprawdzTekst(nowy.wlas);
    printf("nowy wlasciciel to %s \n", nowy.wlas);
    strcpy(temp->wlas, nowy.wlas);
    }

    if(komenda == 6){
    puts("Podaj przebieg");
    nowy.przebieg = sprawdzKomende(PRZEBIEG_MIN, PRZEBIEG_MAX);
    temp ->przebieg = nowy.przebieg;
    }
    free(BazaMarki);
    puts("Operacja zakonczona");
}

void CzyscBaze(Samochod ** glowa)
{
    while(*glowa)
    {
        UsunSamochod(&*glowa, 1);
    }
}

void AktualizujBaze(Samochod ** glowa, Marka usun, char nowaNzw[DL_TXT], int opcja)
{
    Samochod * temp = malloc(sizeof(Samochod));
    temp = *glowa;
    int i = 1;
    while(temp)
    {
        if(!strcmp(temp->typ.nazwa, usun.nazwa))
           {
               if(opcja == 1){
               strcpy(temp->typ.nazwa, nowaNzw);
               temp = temp->nast;
               i++;
               }
               if(opcja == 0)
               {
                temp = temp->nast;
                UsunSamochod(&*glowa, i);
               }
           }
           else{
           temp = temp->nast;
           i++;
           }
    }
}
