#ifndef MARKA_H_INCLUDED
#define MARKA_H_INCLUDED



#define DL_TXT 25

struct Marka{
    char nazwa[DL_TXT];
    int num;
    struct Marka* nast;
};

typedef struct Marka Marka;

void PokazMarka();
Marka* EdytujMarka();
Marka * WczytajMarka();
void ZapiszMarka();
void DodajMarka();
void ZwolnijMarka();
void SprawdzMarka();
void ZmienMarka();
void UsunMarka();
int SprNzwMrk();

#endif // MARKA_H_INCLUDED
