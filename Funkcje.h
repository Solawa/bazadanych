#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED


int sprawdzKomende();
int PorRok();
int PorCena();
int PorPrzeb();
int PorMod();
int PorWlas();
int PorMar();
int Sprawdz();
void CzyscMenu();
void MenuZmianyMarki();
void PrtDuze();
void PrtDuzeBez();
void MenuEdytSam();
void pokapr();

#endif // MENU_H_INCLUDED
