#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Samochod.h"
#include "Funkcje.h"

int PorRok(Samochod * jeden, Samochod * dwa)
{
    if(jeden->rok > dwa->rok)
        return -1;
    if(jeden->rok == dwa->rok)
        return 0;
    else
        return 1;
}

int PorCena(Samochod * jeden, Samochod * dwa)
{
    if(jeden->cena > dwa->cena)
        return -1;
    if(jeden->cena == dwa->cena)
        return 0;
    else
        return 1;
}
int PorPrzeb(Samochod * jeden, Samochod * dwa)
{
    if(jeden->przebieg > dwa->przebieg)
        return -1;
    if(jeden->przebieg == dwa->przebieg)
        return 0;
    else
        return 1;
}

int PorMod(Samochod * jeden, Samochod * dwa)
{
    if(strcmp(jeden->model, dwa->model)< 0)
        return -1;
    if(strcmp(dwa->model, jeden->model)== 0)
        return 0;
    else
        return 1;
}

int PorWlas(Samochod * jeden, Samochod * dwa)
{
    if(strcmp(jeden->wlas, dwa->wlas)< 0)
        return -1;
    if(strcmp(dwa->wlas, jeden->wlas)== 0)
        return 0;
    else
        return 1;
}

int PorMar(Samochod * jeden, Samochod * dwa)
{
    if(strcmp(jeden->typ.nazwa, dwa->typ.nazwa)< 0)
        return -1;
    if(strcmp(dwa->typ.nazwa, jeden->typ.nazwa)== 0)
        return 0;
    else
        return 1;
}


void CzyscMenu()
{
    #ifdef _WIN32
    system("cls");
    #else
    system("clear");
    #endif // _WIN32
}

void MenuEdytSam()
{
    puts("Co chcesz edytowac");
    puts("1.Marka");
    puts("2.Model");
    puts("3.Rok");
    puts("4.Cena");
    puts("5.Wlasciciel");
    puts("6.Przebieg");
}

int sprawdzKomende(const int MINKOM, const int MAXKOM)
{
    int wej;
    while(!(scanf("%d", &wej)))
    {
        while(getchar()!='\n'){}
        printf("Prosze podac cyfre\n");
    }
    getchar();
    if(MINKOM > wej || wej > MAXKOM)
    {
    pokapr("Podaj odpowiednia liczbe ", MINKOM, MAXKOM);
    sprawdzKomende(MINKOM, MAXKOM);
    }
    else{
    return wej;
    }
}

int Sprawdz(const int MIN, const int MAX)
{
    //printf("Podaj liczbe z przedzialu %d - %d\n", MIN, MAX);
    int rok;
    while(!scanf("%d", &rok))
    {
        while(getchar()!='\n');
        puts("Podaj liczbe");
    }
    getchar();
    if(rok>= MIN && rok <=MAX)
        return rok;
    puts("Podales zla liczbe. Podaj jeszcze raz");
    Sprawdz(MIN, MAX);
}

void pokapr(char txt[], int min, int max)
{
    printf("%s", txt);
    PrtDuzeBez(max);
    printf(" - ");
    PrtDuzeBez(min);
    printf("\n");
}

void PrtDuze(int a)
{
    int milion = 1000000;
    int stoTys = 100000;
    int dzTys = 10000;
    int Tys = 1000;
    if(a / milion)
        printf("%3d,", a / milion);
    else{
        printf("    " );
    }
    if(stoTys <=a)
        printf("%03d,", (a/Tys)%Tys);
    else
        printf(" ");
    if(stoTys > a && dzTys<=a)
        printf("%02d,", (a/Tys)%Tys);
    if(dzTys > a && Tys<=a)
        printf("%01d,", (a/Tys)%Tys);
    printf("%03d", a%Tys);
}
void PrtDuzeBez(int a)
{
    int milion = 1000000;
    int stoTys = 100000;
    int dzTys = 10000;
    int Tys = 1000;
    if(a / milion)
        printf("%3d,", a / milion);
    if(stoTys <=a)
        printf("%03d,", (a/Tys)%Tys);
    if(stoTys > a && dzTys<=a)
        printf("%02d,", (a/Tys)%Tys);
    if(dzTys > a && Tys<=a)
        printf("%01d,", (a/Tys)%Tys);
    printf("%03d", a%Tys);
}
