#include "Marka.h"
#include "Samochod.h"
#include "Funkcje.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

void PokazMarka(Marka* glowa)
{
    Marka * temp = malloc(sizeof(Marka));
    temp = glowa;
    puts("Oto marki:");
    while(temp)
    {
        printf("%s \n", temp->nazwa);
        //printf("%s %d %d %d \n", temp->nazwa, temp, glowa, temp->nast);
        temp = temp->nast;
    }
}

Marka * WczytajMarka(char NazwaPlik[])
{
    Marka * glowa = malloc(sizeof(Marka));
    Marka * wczyt = malloc(sizeof(Marka));
    glowa = NULL;

    FILE *plik;
    plik = fopen(NazwaPlik, "r");

    if(!plik){
        printf("Blad\n");
    }
    else{
        while(fscanf(plik, "%s", wczyt->nazwa)==1)
        {
            DodajMarka(&glowa, wczyt);
        }
    }
    free(wczyt);
    fclose(plik);
    return glowa;
}

void ZapiszMarka(char NazwaPlik[], Marka* glowa)
{
    FILE *plik;
    plik = fopen(NazwaPlik, "w");
    Marka * temp = malloc(sizeof(Marka));
    temp = glowa;
    if(!plik)
    {
        printf("Blad\n");
        return ;
    }
    else{
            while(temp)
            {
                fprintf(plik, "%s ", temp->nazwa);
                temp=temp->nast;
            }
    }
    free(temp);
    fclose(plik);
}

void DodajMarka(Marka ** glowa, Marka* nowa)
{
    Marka * temp = malloc(sizeof(Marka));
    Marka ** ost = malloc(sizeof(Marka));
    for(ost = &*glowa; *ost; ost = &(*ost)->nast);

    strcpy(temp->nazwa, nowa->nazwa);
    *ost = temp;
    temp->nast = NULL;
}

void ZwolnijMarka(Marka ** glowa)
{
    Marka * temp = malloc(sizeof(Marka));
    while((*glowa)->nast)
    {
        temp = *glowa;
        *glowa = temp->nast;
        free(temp);
    }
}

void SprawdzMarka(Marka * nowy, Marka * glowa)
{
    PokazMarka(glowa);
    Marka * temp = malloc(sizeof(Marka));
    temp = glowa;
    char nazwa[DL_TXT];
    puts("Podaj nazwe marki");
    scanf("%s", nazwa);
    getchar();
    while(temp->nast && strcmp(nazwa, temp->nazwa))
    {
        temp = temp -> nast;
    }
    if(strcmp(nazwa, temp->nazwa)){
        puts("Podales marki ktorej nie ma w bazie");
        SprawdzMarka(nowy, glowa);
        return;
    }
    //puts("Marka wczytana poprawnie");
   // puts("Wcisnij dowolny klawisz aby kontynulowac");
    strcpy(nowy->nazwa, nazwa);
   // while(getchar()!='\n');
}

void ZmienMarka(Samochod ** Baza)
{
    Marka * BazaMarki = malloc(sizeof(Marka));
    BazaMarki = WczytajMarka("Mark.txt");
    int komenda;
    const int Max_Kom = 4;
    const int Min_Kom = 0;
    do{
        CzyscMenu();
        ZapiszMarka("Mark.txt", BazaMarki);
        void MenuZmianyMarki();
        puts("1.Usun element");
        puts("2.Edytuj element");
        puts("3.Dodaj nowy element");
        puts("4.Pokaz marki");
        komenda = Sprawdz(Min_Kom, Max_Kom);
        switch(komenda)
        {
        case 1://Usun element
            puts("Komenda Wczytana");
            Marka usun;
            SprawdzMarka(&usun, BazaMarki);
            UsunMarka(usun, &BazaMarki);
            AktualizujBaze(&*Baza, usun, "Brak",0);//0 to usuwaj
            //ZapiszMarka("Mark.txt", BazaMarki);
            break;
        case 2:
            puts("Edytowanie marki");
            puts("Podaj nazwe marki do edycji");
            Marka edyt;
            SprawdzMarka(&edyt, BazaMarki);
            Marka * nowa;
            nowa = EdytujMarka(&BazaMarki, &edyt);
            AktualizujBaze(&*Baza, edyt, nowa->nazwa,1);// 1 to edytuj
            break;
        case 3:
            puts("Dodawanie marki");
            puts("Podaj nazwe nowej marki");
            Marka dod;
            do{
                scanf("%s", dod.nazwa);
                getchar();
            }while(!SprNzwMrk(dod.nazwa));
            DodajMarka(&BazaMarki, &dod);
            break;
        case 4:
            PokazMarka(BazaMarki);
            while(getchar()!= '\n');
            break;

        }
    }while(komenda);

    free(BazaMarki);
}

void UsunMarka(Marka usun, Marka ** glowa)
{
    Marka ** temp = malloc(sizeof(Marka));
    for(temp = &*glowa; *temp ; temp = &(*temp)->nast)
    {
        if(!strcmp((*temp)->nazwa, usun.nazwa))
        {
            *temp=(*temp)->nast;
            break;
        }
    }

}

Marka* EdytujMarka(Marka ** glowa, Marka * edyt)
{
    Marka * temp = malloc(sizeof(Marka));
    temp = *glowa;
    while(strcmp(temp->nazwa, edyt->nazwa)!=0)
    {
        temp = temp->nast;
    }
    printf("Obecnie %s \n", temp->nazwa);
    puts("Podaj nowa nazwe marki");
    do{
    scanf("%s", temp->nazwa);
    getchar();
    }while(!SprNzwMrk(temp->nazwa));
    printf("Nowa %s \n", temp->nazwa);
    return temp;
}

int SprNzwMrk(char nazwa[DL_TXT])
{
    for(int i=0; strlen(nazwa) > i; i++)
    {
        if(isdigit(nazwa[i]))
        {
        puts("Podaj nazwe bez cyfr");
        return 0;
        }
    }
    return 1;
}

