#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Samochod.h"
#include "Funkcje.h"

int main()
{
    const char roz[] = {".txt"};
    const int MINKOM = 0;
    const int MAXKOM = 10;
    const int MAX_ELE = 2000000000;
    typedef int (*SamFun)(Samochod*, Samochod*);
    int komenda;
    Samochod * BazaSamoch = malloc(sizeof(Samochod));
    BazaSamoch = WczytajBaze("BazaModel.txt");
    SamFun por[] = {&PorRok, &PorCena, &PorPrzeb, &PorMod, &PorWlas, &PorMar};

    puts("############################################");
    puts("####Projekt 3 i 4. Adam Solawa. AIR 1 sem###");
    puts("####  Baza Danych Salonu Samochodowego  ####");
    puts("############################################");
    puts("");
    puts("Nacisnij dowolny klawisz aby przejsc dalej");
    while(getchar()!= '\n');
    do{
        CzyscMenu();

        puts("0.Koniec");
        puts("1.Dodaj rekord");
        puts("2.Usun rekord");
        puts("3.Edytuj rekord");
        puts("4.Sortuj baze");
        puts("5.Pokaz baze");
        puts("6.Czysc baze");
        puts("7.Zmien marki");
        puts("8.Zapisz baze");
        puts("9.Wczytaj baze");
        puts("10.Czysc ekran");

        puts("Prosze podac komende");
        komenda = Sprawdz(MINKOM, MAXKOM);
        //puts("Komenda wczytana z sukcesem");
        // while(getchar()!= '\n');
        CzyscMenu();
        switch(komenda)
        {
        case 0:
            komenda = 0;
            break;

        case 1:
            puts("Dodawanie elementu");
            Samochod nowy = WczytajElement();
            Dodaj(&BazaSamoch, &nowy);
            break;
        case 2: // Usun
            if(BazaSamoch){
                do{
                    CzyscMenu();
                    PokazBaze(BazaSamoch);
                    puts("Podaj numer elementu");
                    int num_us = Sprawdz(1, MAX_ELE);
                    UsunSamochod(&BazaSamoch,num_us);
                    puts("Czy jeszcze raz? 1. Tak 0. Nie");
                }while(Sprawdz(0,1));
            }
            break;

        case 3://Edytuj
            do{
                CzyscMenu();
                PokazBaze(BazaSamoch);
                puts("Podaj numer elementu");
                int num_edyt = Sprawdz(1, MAX_ELE);
                puts("Podaj co chcesz edytowac");
                puts("Co chcesz edytowac");
                puts("1.Marka");
                puts("2.Model");
                puts("3.Rok");
                puts("4.Cena");
                puts("5.Wlasciciel");
                puts("6.Przebieg");
                int edyt_kom = sprawdzKomende(0, 6);
                EdytSamochod(&BazaSamoch,num_edyt, edyt_kom);
                puts("Operacja wykonana");
                puts("Czy jeszcze raz? 1. Tak 0. Nie");
            }while(sprawdzKomende(0,1));
            break;

        case 4:
            CzyscMenu();
            PokazBaze(BazaSamoch);

            puts("Po czym sortowac");
            puts("1.Rok");
            puts("2.Cena");
            puts("3.Przebieg");
            puts("4.Model");
            puts("5.Wlascicel");
            puts("6.Marka");

            int rodzSort = Sprawdz(0,6);
            if(rodzSort)
            Sort(&BazaSamoch, por[rodzSort-1]);
            puts("Sortowanie zakonczonie sukcesem");
            puts("Nacisnij dowolny klawisz aby przejsc dalej");
            while(getchar()!= '\n');
            break;

        case 5:
            PokazBaze(BazaSamoch);
            puts("Aby wrocic wcisnij dowolny klawisz");
            while(getchar()!= '\n');
            break;

        case 6://Czysc Baze
            CzyscBaze(&BazaSamoch);
            puts("Baza wyczyszczona");
            while(getchar()!= '\n');
            break;

        case 7://Zmien Marka
            ZmienMarka(&BazaSamoch);
            break;

        case 8://zapis
            puts("Podaj nazwe pliku do zapisu");
            char nazwa_zp[DL_TXT];
            scanf("%s", nazwa_zp);
            strcat(nazwa_zp, roz);
            getchar();
            ZapiszBaze(nazwa_zp, BazaSamoch);
            break;
        case 9:
            puts("Podaj nazwe bazy do wczytania");
            char nazwa_wcz[DL_TXT];
            scanf("%s", nazwa_wcz);
            strcat(nazwa_wcz, roz);
            getchar();
            BazaSamoch = WczytajBaze(nazwa_wcz);
            puts("Nacisnij dowolny klawisz aby przejsc dalej");
            while(getchar()!= '\n');
            break;
        case 10:
            CzyscMenu();
            break;

        }
    }
    while(komenda);

}


